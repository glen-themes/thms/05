![Screenshot preview of the theme "Asphyxia" by glenthemes](./asphyxia_pv.gif)

**Theme no.:** 05  
**Theme name:** Asphyxia  
**Theme type:** Free / Tumblr use  
**Description:** A full sidebar theme featuring Kaneki Ken from Tokyo Ghoul. Light and dark versions are available separately, though they share the same options.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-08-25](https://cdn.discordapp.com/attachments/382037367940448256/1101311869496598669/asphyxia_2015_dual.gif)  
**Rework date:** 2021-11-06

**Post:** [glenthemes.tumblr.com/post/127560381809](https://glenthemes.tumblr.com/post/127560381809)  
**Preview [LIGHT]:** [glenthpvs.tumblr.com/asphyxialight](https://glenthpvs.tumblr.com/asphyxialight)  
**Preview [DARK]:** [glenthpvs.tumblr.com/asphyxiadark](https://glenthpvs.tumblr.com/asphyxiadark)  
**Download [LIGHT]:** [pastebin.com/5sALi8Pf](https://pastebin.com/5sALi8Pf)  
**Download [DARK]:** [pastebin.com/zxR1cmC6](https://pastebin.com/zxR1cmC6)  
**Credits:** [glencredits.tumblr.com/asphyxia](https://glencredits.tumblr.com/asphyxia)
